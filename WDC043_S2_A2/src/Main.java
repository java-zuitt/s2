import java.util.Arrays;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
        int[] intArray = new int[5];
        intArray[0] = 2;
        intArray[1] = 3;
        intArray[2] = 4;
        intArray[3] = 5;
        intArray[4] = 6;
        System.out.println("The first prime number is: " + intArray[0]);
        System.out.println("The second prime number is: " + intArray[1]);
        System.out.println("The third prime number is: " + intArray[2]);
        System.out.println("The fourth prime number is: " + intArray[3]);
        System.out.println("The fifth prime number is: " + intArray[4]);

        String[] friends = {"Juan","Dos","Tres","Quatro"};
        System.out.println("My friends are: " + Arrays.toString(friends));

        HashMap<String,String> groceries = new HashMap<>(){
            {
                put("toothpaste","15");
                put("toothbrush","20");
                put("soap","12");
            }
        };
        System.out.println("Our current inventory consists of: " + groceries);

    }
}