package com.zuitt;

import java.util.Scanner;

public class CLIApplication {
    public static void main(String[] args){

        Scanner year = new Scanner(System.in);
        System.out.println("Input year to be checked if a leap year.");
        int input = year.nextInt();

        if(input % 4 == 0 && (input % 100 != 0 || input % 400 == 0)){
            System.out.println(input + " is a leap year.");
        } else {
            System.out.println(input + " is not a leap year.");
        }

    }
}
